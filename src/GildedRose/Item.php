<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 07.05.2017
 * Time: 23:26
 */

namespace GildedRose;

class Item
{
    public $name;
    public $sellIn;
    public $quality;

    public function __construct($name, $quality, $sellIn)
    {
        $this->name     = $name;
        $this->quality  = $quality;
        $this->sellIn   = $sellIn;
    }

    public function updateQuality()
    {
        $this->quality -= 1;
        $this->sellIn -= 1;

        if( $this->sellIn <= 0)
            $this->quality -= 1;
    }
}