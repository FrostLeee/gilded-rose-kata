<?php

namespace GildedRose;

class GildedRose
{
    public static function data($name, $quality, $sellIn)
    {
        switch ($name)
        {
            case "Aged Brie": // Если имя имеет значение "Aged Brie", вызываем класс AgedBrie()
                return new AgedBrie($name, $quality, $sellIn);

            case "Backstage passes to a TAFKAL80ETC concert": // Если имя имеет значение "Backstage passes to a TAFKAL80ETC concert", вызываем класс Backstage()
                return new Backstage($name, $quality, $sellIn);

            case "Sulfuras, Hand of Ragnaros": // Если имя имеет значение "Sulfuras, Hand of Ragnaros", вызываем класс Sulfuras()
                return new Sulfuras($name, $quality, $sellIn);

            default: // Если имя не имеет одно из перечисленных имен, то вызываем класс Item()
                return new Item($name, $quality, $sellIn);
        }
    }
}

