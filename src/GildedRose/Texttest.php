<?php
namespace GildedRose;

class Texttest
{
    public static function start()
    {
        echo "OMGHAI!\n";

        $items = [
            GildedRose::data('+5 Dexterity Vest', 10, 20),
            GildedRose::data('Aged Brie', 2, 0),
            GildedRose::data('Elixir of the Mongoose', 5, 7),
            GildedRose::data('Sulfuras, Hand of Ragnaros', 0, 80),
            GildedRose::data('Sulfuras, Hand of Ragnaros', -1, 80),
            GildedRose::data('Backstage passes to a TAFKAL80ETC concert', 15, 20),
            GildedRose::data('Backstage passes to a TAFKAL80ETC concert', 10, 49),
            GildedRose::data('Backstage passes to a TAFKAL80ETC concert', 5, 49),
        ];
        $days = 3;

        for ($i = 0; $i < $days; $i++)
        {
            echo("-------- day $i --------\n");
            echo sprintf("- %42s - %2s - %s\n", "name", "sellIn", "quality");
            foreach ($items as $item)
            {
                echo sprintf("- %42s - %6d - %d\n", $item->name, $item->sellIn, $item->quality);
                $item->updateQuality();
            }
        }
    }
}
