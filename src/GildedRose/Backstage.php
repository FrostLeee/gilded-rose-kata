<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 07.05.2017
 * Time: 23:53
 */

namespace GildedRose;

class Backstage
{
    public $name;
    public $quality;
    public $sellIn;

    public function __construct($name, $quality, $sellIn)
    {
        $this->name     = $name;
        $this->quality  = $quality;
        $this->sellIn   = $sellIn;
    }

    public function updateQuality()
    {
        $this->quality += 1;

        if ($this->sellIn <= 10) // Если sellIn меньше или равен 10, то увеличиваем его на 1
            $this->quality += 1;

        if($this->sellIn <= 5) // Если sellIn меньше или равен 5, то увеличиваем его на 1
            $this->quality += 1;

        if($this->sellIn <= 0) // Если sellIn меньше или равен 0, то присваиваем ему 0
            $this->quality = 0;

        if($this->quality > 50) // Если quality > 50, то присваиваем ему 50
            $this->quality = 50;

        $this->sellIn -= 1;
    }
}