<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 07.05.2017
 * Time: 23:54
 */

namespace GildedRose;

class Sulfuras
{
    public $name;
    public $quality;
    public $sellIn;

    public function __construct($name, $quality, $sellIn)
    {
        $this->name     = $name;
        $this->quality  = $quality;
        $this->sellIn   = $sellIn;
    }

    public function updateQuality()
    {

    }
}