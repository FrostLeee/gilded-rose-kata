<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 07.05.2017
 * Time: 23:51
 */

namespace GildedRose;

class AgedBrie
{
    public $name;
    public $quality;
    public $sellIn;

    public function __construct($name, $quality, $sellIn)
    {
        $this->name     = $name;
        $this->quality  = $quality;
        $this->sellIn   = $sellIn;
    }

    public function updateQuality()
    {
        $this->quality += 1;
        $this->sellIn -= 1;

        if ($this->sellIn <= 0) // Если sellIn меньше или равен 0, то прибавляем ему 1
            $this->quality += 1;

        if($this->quality > 50) // Если quality больше 50, то присваиваем ему 50
            $this->quality = 50;
    }
}